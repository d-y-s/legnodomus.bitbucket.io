function formTestPopup() {



    $('#area label').on('click', function () {
        $('#area .button').removeClass('false');
    });
    $('#format label').on('click', function () {
        $('#format .button').removeClass('false');
    });
    $('#foundation label').on('click', function () {
        $('#foundation .button').removeClass('false');
    });
    $('#window label').on('click', function () {
        $('#window .button').removeClass('false');
    });
    $('#floor label').on('click', function () {
        $('#floor .button').removeClass('false');
    });
    $('#coating label').on('click', function () {
        $('#coating .button').removeClass('false');
    });
    $('#addition label').on('click', function () {
        $('#addition .button').removeClass('false');
    });
    $('#station label').on('click', function () {
        $('#station .button').removeClass('false');
    });



    var intNumber = 0,
        test, area, format, foundation, window, floor, coating, addition, station;
    $('.back_button').on('click', function () {
        var back = $(this).attr('attr-back');
        $('.form-content').css('display', 'none');
        $('#' + back).css('display', 'block');
        intNumber--;
    });

    $('#form .button').on('click', function (e) {
        e.preventDefault();
        if (intNumber == 0) {
            if ($("#area input:checked").val()) {
                area = $("#area input:checked").attr('data-area');
                intNumber++;
                $('.step-content .step-item:nth-child(2)').addClass('active');
                $('.step-text span').text('2');
                $('#area').css('display', 'none');
                $('.line .green').css('width', '12%');
                $('#format').css('display', 'block');
                $('.line .price').text('8000')
            } else {
                $('#area .button').addClass('false')
            }
        }
        if (intNumber == 1) {
            if ($("#format input:checked").val()) {
                format = $("#format input:checked").attr('format');
                $('#foundation').css('display', 'block');
                $('#format').css('display', 'none');
                intNumber++;
                $('.step-content .step-item:nth-child(3)').addClass('active');
                $('.step-text span').text('3');
                $('.line .green').css('width', '24%');
                $('.line .price').text('12000');
                return
            }
        }
        if (intNumber == 2) {
            if ($("#foundation input:checked").val()) {
                $('#foundation').css('display', 'none');
                foundation = $("#foundation input:checked").attr('foundation');
                $('.step-content .step-item:nth-child(4)').addClass('active');
                $('.step-text span').text('4');
                $('#window').css('display', 'block');
                $('.line .green').css('width', '36%');
                $('.line .price').text('16000');
                intNumber++;
                return
            }
        }
        if (intNumber == 3) {
            if ($("#window input:checked").val()) {
                $('#window').css('display', 'none');
                window = $("#window input:checked").attr('window');
                $('.step-content .step-item:nth-child(5)').addClass('active');
                $('.step-text span').text('5');
                $('#floor').css('display', 'block');
                $('.line .green').css('width', '48%');
                $('.line .price').text('20000');
                intNumber++;
                return
            }
        }
        if (intNumber == 4) {
            if ($("#floor input:checked").val()) {
                $('#floor').css('display', 'none');
                floor = $("#floor input:checked").attr('floor');
                $('.step-content .step-item:nth-child(6)').addClass('active');
                $('.step-text span').text('6');
                $('#coating').css('display', 'block');
                $('.line .green').css('width', '60%');
                $('.line .price').text('24000');
                intNumber++;
                return
            }
        }
        if (intNumber == 5) {
            if ($("#coating input:checked").val()) {
                $('#coating').css('display', 'none');
                coating = $("#coating input:checked").attr('coating');
                $('.step-content .step-item:nth-child(7)').addClass('active');
                $('.step-text span').text('7');
                $('#addition').css('display', 'block');
                $('.line .green').css('width', '72%');
                $('.line .price').text('28000');
                intNumber++;
                return
            }
        }
        if (intNumber == 6) {
            if ($("#addition input:checked").val()) {
                $('#addition').css('display', 'none');
                addition = $("#addition input:checked").attr('addition');
                $('.step-content .step-item:nth-child(8)').addClass('active');
                $('.step-text span').text('8');
                $('#station').css('display', 'block');
                $('.line .green').css('width', '84%');
                $('.line .price').text('32000');
                intNumber++;
                return
            }
        }
        if (intNumber == 7) {
            if ($("#station input:checked").val()) {
                $('#station').css('display', 'none');
                station = $("#station input:checked").attr('station');
                $('.step-content .step-item:nth-child(9)').addClass('active');
                $('.step-text span').text('9');
                $('#goods').css('display', 'block');
                $('.line .green').css('width', '100%');
                intNumber++;
                return
            }
        }
        if (intNumber == 8) {
            debugger;
            var name, tel, mail;
            name = $('#form input.name').val();
            tel = $('#form input.tel').val();
            mail = $('#form input.mail').val();
            if ((mail != '') && (tel != '')) {
                jQuery.ajax({
                    url: 'mail.php',
                    type: 'POST',
                    cache: !1,
                    data: {
                        'name': name,
                        'tel': tel,
                        'mail': mail,
                        'area': area,
                        'format': format,
                        'foundation': foundation,
                        'window': window,
                        'floor': floor,
                        'coating': coating,
                        'addition': addition,
                        'station': station
                    },
                    dataType: 'html',
                    success: function (data) {
                        $('.form-top').css('display', 'none');
                        $('.line').css('display', 'none');
                        $('.step').css('display', 'none');
                        $('.form-content').css('display', 'none');
                        $('.finish').css('display', 'block');
                        setTimeout(function () {
                            location = "https://legnodomus.ru"
                        }, 25000);
                        yaCounter51931073.reachGoal('finishQuiz');
                    }
                })
            } else {
                $('#form form input.mail').css('border', '1px red solid');
            }
        }
        console.log(intNumber)
    });


    $('#callback .button').on('click', function () {
        var name, tel, mail;
        name = $('#callback .name').val();
        tel = $('#callback .tel').val();
        mail = $('#callback .mail').val();
        if ((mail != '') && (tel != '')) {
            jQuery.ajax({
                url: 'send.php',
                type: 'POST',
                cache: !1,
                data: {
                    'name': name,
                    'tel': tel,
                    'mail': mail
                },
                dataType: 'html',
                success: function (data) {
                    $('#callback .form_content').css('display', 'none');
                    $('#callback .ok').css('display', 'block');
                    yaCounter51931073.reachGoal('callfinish');
                }
            })
        } else {
            $('#callback .mail').css('border', '1px red solid')
        }

    });

    $('.js-btn-take-test').on('click', function () {
        $(this).preventDefault;
        $('.js-form-test-popup').fadeIn();
    });

    $('.fancybox-button.fancybox-close-small').on('click', function () {
        $('.js-form-test-popup').fadeOut();
    });

}


formTestPopup();
