"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  var _productSlider$owlCar;

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.js-call-modal-call').on('click', function () {
    $('.js-call-modal').addClass('is-show-modal');
    $('body').addClass('is-overley');
  });
  $('.js-btn-close').on('click', function () {
    $('.js-call-modal').removeClass('is-show-modal');
    $('body').removeClass('is-overley');
  });
  $('.common-tabs__btn-items').on('click', '.js-btn-tab:not(.is-active-btn-tab)', function () {
    $(this).addClass('is-active-btn-tab').siblings().removeClass('is-active-btn-tab');
    $('.common-tabs__container').find('.js-tab-content').removeClass('is-active-tabs-content').hide().eq($(this).index()).fadeIn(); // $('.projects__item').removeClass('js-animate-scroll-reveal').removeAttr('style')
    // $('.projects__item').addClass('js-animate-scroll-reveal')

    ScrollReveal().destroy();
  });
  $('.js-faq-item').on('click', function () {
    $(this).toggleClass('is-active');
    $(this).find('.js-faq-content').stop().slideToggle();
  });
  $('.js-filter-name').on('click', function () {
    $(this).toggleClass('is-active-filter-name');
    $(this).siblings('.js-check-items').stop().slideToggle(100);
  });
  $('.js-filter-title').on('click', function () {
    $(this).siblings('.filter__items').stop().slideToggle(100);
  });
  $(window).on('load resize', function () {
    if ($(window).width() >= 640) {
      $('.filter__items').css('display', 'flex');
    }
  });

  function formTestPopup() {
    $('#area label').on('click', function () {
      $('#area .button').removeClass('false');
    });
    $('#format label').on('click', function () {
      $('#format .button').removeClass('false');
    });
    $('#foundation label').on('click', function () {
      $('#foundation .button').removeClass('false');
    });
    $('#window label').on('click', function () {
      $('#window .button').removeClass('false');
    });
    $('#floor label').on('click', function () {
      $('#floor .button').removeClass('false');
    });
    $('#coating label').on('click', function () {
      $('#coating .button').removeClass('false');
    });
    $('#addition label').on('click', function () {
      $('#addition .button').removeClass('false');
    });
    $('#station label').on('click', function () {
      $('#station .button').removeClass('false');
    });
    var intNumber = 0,
        test,
        area,
        format,
        foundation,
        window,
        floor,
        coating,
        addition,
        station;
    $('.back_button').on('click', function () {
      var back = $(this).attr('attr-back');
      $('.form-content').css('display', 'none');
      $('#' + back).css('display', 'block');
      intNumber--;
    });
    $('#form .button').on('click', function (e) {
      e.preventDefault();

      if (intNumber == 0) {
        if ($("#area input:checked").val()) {
          area = $("#area input:checked").attr('data-area');
          intNumber++;
          $('.step-content .step-item:nth-child(2)').addClass('active');
          $('.step-text span').text('2');
          $('#area').css('display', 'none');
          $('.line .green').css('width', '12%');
          $('#format').css('display', 'block');
          $('.line .price').text('8000');
        } else {
          $('#area .button').addClass('false');
        }
      }

      if (intNumber == 1) {
        if ($("#format input:checked").val()) {
          format = $("#format input:checked").attr('format');
          $('#foundation').css('display', 'block');
          $('#format').css('display', 'none');
          intNumber++;
          $('.step-content .step-item:nth-child(3)').addClass('active');
          $('.step-text span').text('3');
          $('.line .green').css('width', '24%');
          $('.line .price').text('12000');
          return;
        }
      }

      if (intNumber == 2) {
        if ($("#foundation input:checked").val()) {
          $('#foundation').css('display', 'none');
          foundation = $("#foundation input:checked").attr('foundation');
          $('.step-content .step-item:nth-child(4)').addClass('active');
          $('.step-text span').text('4');
          $('#window').css('display', 'block');
          $('.line .green').css('width', '36%');
          $('.line .price').text('16000');
          intNumber++;
          return;
        }
      }

      if (intNumber == 3) {
        if ($("#window input:checked").val()) {
          $('#window').css('display', 'none');
          window = $("#window input:checked").attr('window');
          $('.step-content .step-item:nth-child(5)').addClass('active');
          $('.step-text span').text('5');
          $('#floor').css('display', 'block');
          $('.line .green').css('width', '48%');
          $('.line .price').text('20000');
          intNumber++;
          return;
        }
      }

      if (intNumber == 4) {
        if ($("#floor input:checked").val()) {
          $('#floor').css('display', 'none');
          floor = $("#floor input:checked").attr('floor');
          $('.step-content .step-item:nth-child(6)').addClass('active');
          $('.step-text span').text('6');
          $('#coating').css('display', 'block');
          $('.line .green').css('width', '60%');
          $('.line .price').text('24000');
          intNumber++;
          return;
        }
      }

      if (intNumber == 5) {
        if ($("#coating input:checked").val()) {
          $('#coating').css('display', 'none');
          coating = $("#coating input:checked").attr('coating');
          $('.step-content .step-item:nth-child(7)').addClass('active');
          $('.step-text span').text('7');
          $('#addition').css('display', 'block');
          $('.line .green').css('width', '72%');
          $('.line .price').text('28000');
          intNumber++;
          return;
        }
      }

      if (intNumber == 6) {
        if ($("#addition input:checked").val()) {
          $('#addition').css('display', 'none');
          addition = $("#addition input:checked").attr('addition');
          $('.step-content .step-item:nth-child(8)').addClass('active');
          $('.step-text span').text('8');
          $('#station').css('display', 'block');
          $('.line .green').css('width', '84%');
          $('.line .price').text('32000');
          intNumber++;
          return;
        }
      }

      if (intNumber == 7) {
        if ($("#station input:checked").val()) {
          $('#station').css('display', 'none');
          station = $("#station input:checked").attr('station');
          $('.step-content .step-item:nth-child(9)').addClass('active');
          $('.step-text span').text('9');
          $('#goods').css('display', 'block');
          $('.line .green').css('width', '100%');
          intNumber++;
          return;
        }
      }

      if (intNumber == 8) {
        debugger;
        var name, tel, mail;
        name = $('#form input.name').val();
        tel = $('#form input.tel').val();
        mail = $('#form input.mail').val();

        if (mail != '' && tel != '') {
          jQuery.ajax({
            url: 'mail.php',
            type: 'POST',
            cache: !1,
            data: {
              'name': name,
              'tel': tel,
              'mail': mail,
              'area': area,
              'format': format,
              'foundation': foundation,
              'window': window,
              'floor': floor,
              'coating': coating,
              'addition': addition,
              'station': station
            },
            dataType: 'html',
            success: function success(data) {
              $('.form-top').css('display', 'none');
              $('.line').css('display', 'none');
              $('.step').css('display', 'none');
              $('.form-content').css('display', 'none');
              $('.finish').css('display', 'block');
              setTimeout(function () {
                location = "https://legnodomus.ru";
              }, 25000);
              yaCounter51931073.reachGoal('finishQuiz');
            }
          });
        } else {
          $('#form form input.mail').css('border', '1px red solid');
        }
      }

      console.log(intNumber);
    });
    $('#callback .button').on('click', function () {
      var name, tel, mail;
      name = $('#callback .name').val();
      tel = $('#callback .tel').val();
      mail = $('#callback .mail').val();

      if (mail != '' && tel != '') {
        jQuery.ajax({
          url: 'send.php',
          type: 'POST',
          cache: !1,
          data: {
            'name': name,
            'tel': tel,
            'mail': mail
          },
          dataType: 'html',
          success: function success(data) {
            $('#callback .form_content').css('display', 'none');
            $('#callback .ok').css('display', 'block');
            yaCounter51931073.reachGoal('callfinish');
          }
        });
      } else {
        $('#callback .mail').css('border', '1px red solid');
      }
    });
    $('.js-btn-take-test').on('click', function () {
      $(this).preventDefault;
      $('.js-form-test-popup').fadeIn();
    });
    $('.fancybox-button.fancybox-close-small').on('click', function () {
      $('.js-form-test-popup').fadeOut();
    });
  }

  formTestPopup();
  $('.js-btn-menu').on('click', function () {
    $(this).toggleClass('is-active-menu');
    $('.js-mobile-menu').toggleClass('is-show-menu');
    $('.js-menu-overlay').toggleClass('is-show-overlay');
  });
  $('.js-btn-show-info').on('click', function () {
    $(this).remove();
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  var productSlider = $('.js-product-preview'),
      productItems = productSlider.find('img'),
      productThumbs = $('.js-product-thumbs');
  $.each(productItems, function (i, el) {
    var classActive = i == 0 ? 'is-active' : '';
    var item = $('<div class="product-slider__thumbs-img ' + classActive + '" data-index="' + i + '"><img src="' + $(el).data('thumb') + '" alt=""></div>');
    item.appendTo(productThumbs);
  });
  productSlider.owlCarousel((_productSlider$owlCar = {
    mouseDrag: false,
    items: 1,
    // animateOut: 'fadeOut',
    nav: false,
    loop: false,
    dots: false
  }, _defineProperty(_productSlider$owlCar, "mouseDrag", false), _defineProperty(_productSlider$owlCar, "touchDrag", false), _defineProperty(_productSlider$owlCar, "animateOut", 'fadeOut'), _defineProperty(_productSlider$owlCar, "navText", ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>']), _defineProperty(_productSlider$owlCar, "onChanged", function onChanged(event) {
    var $thisIndex = event.item.index;
    $('.product-slider__thumbs-img').removeClass('is-active');
    $('.product-slider__thumbs-img[data-index="' + $thisIndex + '"]').addClass('is-active');
  }), _defineProperty(_productSlider$owlCar, "onInitialize", function onInitialize(event) {
    productThumbs.owlCarousel({
      // items: 5,
      margin: 2,
      nav: true,
      loop: true,
      // autoWidth: true,
      navText: ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>'],
      mouseDrag: false,
      responsive: {
        0: {
          items: 3,
          autoWidth: false
        },
        480: {
          autoWidth: true
        }
      }
    });
  }), _productSlider$owlCar));
  $('body').on('click', '.product-slider__thumbs-img', function () {
    productSlider.trigger('to.owl.carousel', [$(this).data('index'), 300]);
    $('.product-slider__thumbs-img').removeClass('is-active');
    $('.product-slider__thumbs-img[data-index="' + $(this).data('index') + '"]').addClass('is-active');
  });

  function ProductSeriesImgPath() {
    $('.product-slider__preview-img').each(function () {
      var getProductSeriesImgPath = $(this).find('img').attr('src'),
          styleInlineBgImg = 'background-image: url(' + getProductSeriesImgPath + ');';
      $(this).attr('style', styleInlineBgImg);
    });
  }

  ProductSeriesImgPath();
  $('.projects__item').each(function () {
    var $this = $(this),
        imgSrc = $this.find('.projects__img img').attr('src'),
        styleInlineBgImg = 'background-image: url(' + imgSrc + ');';
    $this.find('.projects__img').attr('style', styleInlineBgImg);
  });
  $('.js-sorting-current').on('click', function () {
    $('.js-sorting-items').toggleClass('is-show-sorting-items');
  });
  $('.js-recommended-projects').owlCarousel({
    items: 1,
    nav: true,
    dots: false,
    // loop: true,
    onInitialized: initCounterSlide,
    onTranslate: initCounterSlide,
    onTranslated: initCounterSlide,
    navText: ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>']
  });

  function initCounterSlide(event) {
    if (!event.namespace) {
      return;
    }

    var slides = event.relatedTarget;
    $('.js-counter').html(slides.relative(slides.current()) + 1 + '<span>' + '<i>' + '</i>' + slides.items().length + '</i>');
  }

  $('.js-reviews-about-us').owlCarousel({
    items: 3,
    nav: true,
    dots: false,
    // loop: true,
    onInitialized: initCounterSlideReviews,
    onTranslate: initCounterSlideReviews,
    onTranslated: initCounterSlideReviews,
    navText: ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>'],
    responsive: {
      0: {
        items: 1
      },
      881: {
        items: 3
      }
    }
  });

  function initCounterSlideReviews(event) {
    if (!event.namespace) {
      return;
    }

    var slides = event.relatedTarget;
    $('.js-counter-reviews').html(slides.relative(slides.current()) + 1 + '<span>' + '<i>' + '</i>' + slides.items().length + '</i>');
  }

  function explicitlyInteraction() {
    $('.js-main-point').on('click', function (event) {
      event.preventDefault();
      var anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top
      }, 1000);
    });
    $('.js-explicitly-list-item').on('click', function () {
      var $this = $(this);
      var getPoint = $(this).closest('.technology__item--level-1').find('.technology__explicitly-point').eq($this.index());
      $this.addClass('is-active-explicitly-list-item').siblings().removeClass('is-active-explicitly-list-item');
      getPoint.addClass('is-active-explicitly-point').siblings().removeClass('is-active-explicitly-point');
    });
    $('.js-explicitly-list-item').on('click', function () {
      var $this = $(this);
      var getPoint = $(this).closest('.technology__item--level-2').find('.technology__explicitly-point').eq($this.index());
      $this.addClass('is-active-explicitly-list-item').siblings().removeClass('is-active-explicitly-list-item');
      getPoint.addClass('is-active-explicitly-point').siblings().removeClass('is-active-explicitly-point');
    });
    $('.js-explicitly-list-item').on('click', function () {
      var $this = $(this);
      var getPoint = $(this).closest('.technology__item--level-3').find('.technology__explicitly-point').eq($this.index());
      $this.addClass('is-active-explicitly-list-item').siblings().removeClass('is-active-explicitly-list-item');
      getPoint.addClass('is-active-explicitly-point').siblings().removeClass('is-active-explicitly-point');
    });
    $('.js-explicitly-list-item').on('click', function () {
      var $this = $(this);
      var getPoint = $(this).closest('.technology__item--level-4').find('.technology__explicitly-point').eq($this.index());
      $this.addClass('is-active-explicitly-list-item').siblings().removeClass('is-active-explicitly-list-item');
      getPoint.addClass('is-active-explicitly-point').siblings().removeClass('is-active-explicitly-point');
    });
    $('.js-explicitly-list-item').on('click', function () {
      var $this = $(this);
      var getPoint = $(this).closest('.technology__item--level-5').find('.technology__explicitly-point').eq($this.index());
      $this.addClass('is-active-explicitly-list-item').siblings().removeClass('is-active-explicitly-list-item');
      getPoint.addClass('is-active-explicitly-point').siblings().removeClass('is-active-explicitly-point');
    });
    $(".js-explicitly-list-item").click(function () {
      var $this = $(this);
      $('html, body').animate({
        scrollTop: $this.closest('.technology__item').offset().top - 50
      }, 1000);
    });
  }

  explicitlyInteraction();
  $('.js-btn-contacts-top-line').on('click', function () {
    $('.js-contacts-body').toggleClass('is-contacts-show');
  });
  $('input[type="file"], input[type="checkbox"], input[type="radio"], select').not('.form-test-popup input[type="radio"], .form-test-popup input[type="checkbox"]').styler({
    filePlaceholder: 'Выберите файл проекта'
  });
  var slideUp = {
    distance: '150%',
    origin: 'bottom',
    opacity: null
  }; // ScrollReveal().reveal('.headline', slideUp)

  ScrollReveal().reveal('.js-animate-scroll-reveal', {
    delay: 500,
    distance: '50px',
    origin: 'top',
    reset: true
  });
  $(window).on('load resize', function () {
    if ($(window).width() <= 1024) {
      ScrollReveal().destroy();
    }
  });
});