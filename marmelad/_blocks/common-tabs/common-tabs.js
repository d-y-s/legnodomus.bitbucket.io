$('.common-tabs__btn-items').on('click', '.js-btn-tab:not(.is-active-btn-tab)', function () {
    $(this)
        .addClass('is-active-btn-tab')
        .siblings()
        .removeClass('is-active-btn-tab');

    $('.common-tabs__container')
        .find('.js-tab-content')
        .removeClass('is-active-tabs-content')
        .hide().eq($(this).index()).fadeIn();

    // $('.projects__item').removeClass('js-animate-scroll-reveal').removeAttr('style')
    // $('.projects__item').addClass('js-animate-scroll-reveal')
    ScrollReveal().destroy();
});
