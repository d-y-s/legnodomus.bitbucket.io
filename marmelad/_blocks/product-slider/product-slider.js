var productSlider = $('.js-product-preview'),
    productItems = productSlider.find('img'),
    productThumbs = $('.js-product-thumbs');
$.each(productItems, function (i, el) {
    var classActive = i == 0 ? 'is-active' : '';
    var item = $('<div class="product-slider__thumbs-img ' + classActive + '" data-index="' + i + '"><img src="' + $(el).data('thumb') + '" alt=""></div>');
    item.appendTo(productThumbs);
});
productSlider.owlCarousel({
    mouseDrag: false,
    items: 1,
    // animateOut: 'fadeOut',
    nav: false,
    loop: false,
    dots: false,
    mouseDrag: false,
    touchDrag: false,
    animateOut: 'fadeOut',
    navText: ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>'],
    onChanged: function onChanged(event) {
        var $thisIndex = event.item.index;
        $('.product-slider__thumbs-img').removeClass('is-active');
        $('.product-slider__thumbs-img[data-index="' + $thisIndex + '"]').addClass('is-active');
    },
    onInitialize: function onInitialize(event) {
        productThumbs.owlCarousel({
            // items: 5,
            margin: 2,
            nav: true,
            loop: true,
            // autoWidth: true,
            navText: ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>'],
            mouseDrag: false,
            responsive: {
                0: {
                    items: 3,
                    autoWidth: false,
                },
                480: {
                    autoWidth: true,
                },
            }
        });
    }
});
$('body').on('click', '.product-slider__thumbs-img', function () {
    productSlider.trigger('to.owl.carousel', [$(this).data('index'), 300]);
    $('.product-slider__thumbs-img').removeClass('is-active');
    $('.product-slider__thumbs-img[data-index="' + $(this).data('index') + '"]').addClass('is-active');
});

function ProductSeriesImgPath() {
    $('.product-slider__preview-img').each(function () {
        var getProductSeriesImgPath = $(this).find('img').attr('src'),
            styleInlineBgImg = 'background-image: url(' + getProductSeriesImgPath + ');';
        $(this).attr('style', styleInlineBgImg);
    })

}

ProductSeriesImgPath()
