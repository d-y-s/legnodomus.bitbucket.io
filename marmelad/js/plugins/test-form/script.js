function fancyBoxTestForm (e, t, n, o) {
    "use strict";
    function i(e, t) {
        var o, i, r, s = [], a = 0;
        e && e.isDefaultPrevented() || (e.preventDefault(),
            t = t || {},
            e && e.data && (t = h(e.data.options, t)),
            o = t.$target || n(e.currentTarget).trigger("blur"),
            (r = n.fancybox.getInstance()) && r.$trigger && r.$trigger.is(o) || (t.selector ? s = n(t.selector) : (i = o.attr("data-fancybox") || "",
                i ? (s = e.data ? e.data.items : [],
                    s = s.length ? s.filter('[data-fancybox="' + i + '"]') : n('[data-fancybox="' + i + '"]')) : s = [o]),
                a = n(s).index(o),
                a < 0 && (a = 0),
                r = n.fancybox.open(s, t, a),
                r.$trigger = o))
    }
    if (e.console = e.console || {
        info: function (e) { }
    },
        n) {
        if (n.fn.fancybox)
            return void console.info("fancyBox already initialized");
        var r = {
            closeExisting: !1,
            loop: !1,
            gutter: 50,
            keyboard: !0,
            preventCaptionOverlap: !0,
            arrows: !0,
            infobar: !0,
            smallBtn: "auto",
            toolbar: "auto",
            buttons: ["zoom", "slideShow", "thumbs", "close"],
            idleTime: 3,
            protect: !1,
            modal: !1,
            image: {
                preload: !1
            },
            ajax: {
                settings: {
                    data: {
                        fancybox: !0
                    }
                }
            },
            iframe: {
                tpl: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src=""></iframe>',
                preload: !0,
                css: {},
                attr: {
                    scrolling: "auto"
                }
            },
            video: {
                tpl: '<video class="fancybox-video" controls controlsList="nodownload" poster="{{poster}}"><source src="{{src}}" type="{{format}}" />Sorry, your browser doesn\'t support embedded videos, <a href="{{src}}">download</a> and watch with your favorite video player!</video>',
                format: "",
                autoStart: !0
            },
            defaultType: "image",
            animationEffect: "zoom",
            animationDuration: 366,
            zoomOpacity: "auto",
            transitionEffect: "fade",
            transitionDuration: 366,
            slideClass: "",
            baseClass: "",
            baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><div class="fancybox-toolbar">{{buttons}}</div><div class="fancybox-navigation">{{arrows}}</div><div class="fancybox-stage"></div><div class="fancybox-caption"><div class="fancybox-caption__body"></div></div></div></div>',
            spinnerTpl: '<div class="fancybox-loading"></div>',
            errorTpl: '<div class="fancybox-error"><p>{{ERROR}}</p></div>',
            btnTpl: {
                download: '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.62 17.09V19H5.38v-1.91zm-2.97-6.96L17 11.45l-5 4.87-5-4.87 1.36-1.32 2.68 2.64V5h1.92v7.77z"/></svg></a>',
                zoom: '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.7 17.3l-3-3a5.9 5.9 0 0 0-.6-7.6 5.9 5.9 0 0 0-8.4 0 5.9 5.9 0 0 0 0 8.4 5.9 5.9 0 0 0 7.7.7l3 3a1 1 0 0 0 1.3 0c.4-.5.4-1 0-1.5zM8.1 13.8a4 4 0 0 1 0-5.7 4 4 0 0 1 5.7 0 4 4 0 0 1 0 5.7 4 4 0 0 1-5.7 0z"/></svg></button>',
                close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"/></svg></button>',
                arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.28 15.7l-1.34 1.37L5 12l4.94-5.07 1.34 1.38-2.68 2.72H19v1.94H8.6z"/></svg></div></button>',
                arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.4 12.97l-2.68 2.72 1.34 1.38L19 12l-4.94-5.07-1.34 1.38 2.68 2.72H5v1.94z"/></svg></div></button>',
                smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg></button>'
            },
            parentEl: "body",
            hideScrollbar: !0,
            autoFocus: !0,
            backFocus: !0,
            trapFocus: !0,
            fullScreen: {
                autoStart: !1
            },
            touch: {
                vertical: !0,
                momentum: !0
            },
            hash: null,
            media: {},
            slideShow: {
                autoStart: !1,
                speed: 3e3
            },
            thumbs: {
                autoStart: !1,
                hideOnClose: !0,
                parentEl: ".fancybox-container",
                axis: "y"
            },
            wheel: "auto",
            onInit: n.noop,
            beforeLoad: n.noop,
            afterLoad: n.noop,
            beforeShow: n.noop,
            afterShow: n.noop,
            beforeClose: n.noop,
            afterClose: n.noop,
            onActivate: n.noop,
            onDeactivate: n.noop,
            clickContent: function (e, t) {
                return "image" === e.type && "zoom"
            },
            clickSlide: "close",
            clickOutside: "close",
            dblclickContent: !1,
            dblclickSlide: !1,
            dblclickOutside: !1,
            mobile: {
                preventCaptionOverlap: !1,
                idleTime: !1,
                clickContent: function (e, t) {
                    return "image" === e.type && "toggleControls"
                },
                clickSlide: function (e, t) {
                    return "image" === e.type ? "toggleControls" : "close"
                },
                dblclickContent: function (e, t) {
                    return "image" === e.type && "zoom"
                },
                dblclickSlide: function (e, t) {
                    return "image" === e.type && "zoom"
                }
            },
            lang: "en",
            i18n: {
                en: {
                    CLOSE: "Close",
                    NEXT: "Next",
                    PREV: "Previous",
                    ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
                    PLAY_START: "Start slideshow",
                    PLAY_STOP: "Pause slideshow",
                    FULL_SCREEN: "Full screen",
                    THUMBS: "Thumbnails",
                    DOWNLOAD: "Download",
                    SHARE: "Share",
                    ZOOM: "Zoom"
                },
                de: {
                    CLOSE: "Schlie&szlig;en",
                    NEXT: "Weiter",
                    PREV: "Zur&uuml;ck",
                    ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es sp&auml;ter nochmal.",
                    PLAY_START: "Diaschau starten",
                    PLAY_STOP: "Diaschau beenden",
                    FULL_SCREEN: "Vollbild",
                    THUMBS: "Vorschaubilder",
                    DOWNLOAD: "Herunterladen",
                    SHARE: "Teilen",
                    ZOOM: "Vergr&ouml;&szlig;ern"
                }
            }
        }
            , s = n(e)
            , a = n(t)
            , l = 0
            , c = function (e) {
                return e && e.hasOwnProperty && e instanceof n
            }
            , u = function () {
                return e.requestAnimationFrame || e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.oRequestAnimationFrame || function (t) {
                    return e.setTimeout(t, 1e3 / 60)
                }
            }()
            , d = function () {
                return e.cancelAnimationFrame || e.webkitCancelAnimationFrame || e.mozCancelAnimationFrame || e.oCancelAnimationFrame || function (t) {
                    e.clearTimeout(t)
                }
            }()
            , p = function () {
                var e, n = t.createElement("fakeelement"), i = {
                    transition: "transitionend",
                    OTransition: "oTransitionEnd",
                    MozTransition: "transitionend",
                    WebkitTransition: "webkitTransitionEnd"
                };
                for (e in i)
                    if (n.style[e] !== o)
                        return i[e];
                return "transitionend"
            }()
            , f = function (e) {
                return e && e.length && e[0].offsetHeight
            }
            , h = function (e, t) {
                var o = n.extend(!0, {}, e, t);
                return n.each(t, function (e, t) {
                    n.isArray(t) && (o[e] = t)
                }),
                    o
            }
            , g = function (e) {
                var o, i;
                return !(!e || e.ownerDocument !== t) && (n(".fancybox-container").css("pointer-events", "none"),
                    o = {
                        x: e.getBoundingClientRect().left + e.offsetWidth / 2,
                        y: e.getBoundingClientRect().top + e.offsetHeight / 2
                    },
                    i = t.elementFromPoint(o.x, o.y) === e,
                    n(".fancybox-container").css("pointer-events", ""),
                    i)
            }
            , v = function (e, t, o) {
                var i = this;
                i.opts = h({
                    index: o
                }, n.fancybox.defaults),
                    n.isPlainObject(t) && (i.opts = h(i.opts, t)),
                    n.fancybox.isMobile && (i.opts = h(i.opts, i.opts.mobile)),
                    i.id = i.opts.id || ++l,
                    i.currIndex = parseInt(i.opts.index, 10) || 0,
                    i.prevIndex = null,
                    i.prevPos = null,
                    i.currPos = 0,
                    i.firstRun = !0,
                    i.group = [],
                    i.slides = {},
                    i.addContent(e),
                    i.group.length && i.init()
            };
        n.extend(v.prototype, {
            init: function () {
                var o, i, r = this, s = r.group[r.currIndex], a = s.opts;
                a.closeExisting && n.fancybox.close(!0),
                    n("body").addClass("fancybox-active"),
                    !n.fancybox.getInstance() && !1 !== a.hideScrollbar && !n.fancybox.isMobile && t.body.scrollHeight > e.innerHeight && (n("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar{margin-right:' + (e.innerWidth - t.documentElement.clientWidth) + "px;}</style>"),
                        n("body").addClass("compensate-for-scrollbar")),
                    i = "",
                    n.each(a.buttons, function (e, t) {
                        i += a.btnTpl[t] || ""
                    }),
                    o = n(r.translate(r, a.baseTpl.replace("{{buttons}}", i).replace("{{arrows}}", a.btnTpl.arrowLeft + a.btnTpl.arrowRight))).attr("id", "fancybox-container-" + r.id).addClass(a.baseClass).data("FancyBox", r).appendTo(a.parentEl),
                    r.$refs = {
                        container: o
                    },
                    ["bg", "inner", "infobar", "toolbar", "stage", "caption", "navigation"].forEach(function (e) {
                        r.$refs[e] = o.find(".fancybox-" + e)
                    }),
                    r.trigger("onInit"),
                    r.activate(),
                    r.jumpTo(r.currIndex)
            },
            translate: function (e, t) {
                var n = e.opts.i18n[e.opts.lang] || e.opts.i18n.en;
                return t.replace(/\{\{(\w+)\}\}/g, function (e, t) {
                    return n[t] === o ? e : n[t]
                })
            },
            addContent: function (e) {
                var t, i = this, r = n.makeArray(e);
                n.each(r, function (e, t) {
                    var r, s, a, l, c, u = {}, d = {};
                    n.isPlainObject(t) ? (u = t,
                        d = t.opts || t) : "object" === n.type(t) && n(t).length ? (r = n(t),
                            d = r.data() || {},
                            d = n.extend(!0, {}, d, d.options),
                            d.$orig = r,
                            u.src = i.opts.src || d.src || r.attr("href"),
                            u.type || u.src || (u.type = "inline",
                                u.src = t)) : u = {
                                    type: "html",
                                    src: t + ""
                                },
                        u.opts = n.extend(!0, {}, i.opts, d),
                        n.isArray(d.buttons) && (u.opts.buttons = d.buttons),
                        n.fancybox.isMobile && u.opts.mobile && (u.opts = h(u.opts, u.opts.mobile)),
                        s = u.type || u.opts.type,
                        l = u.src || "",
                        !s && l && ((a = l.match(/\.(mp4|mov|ogv|webm)((\?|#).*)?$/i)) ? (s = "video",
                            u.opts.video.format || (u.opts.video.format = "video/" + ("ogv" === a[1] ? "ogg" : a[1]))) : l.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i) ? s = "image" : l.match(/\.(pdf)((\?|#).*)?$/i) ? (s = "iframe",
                                u = n.extend(!0, u, {
                                    contentType: "pdf",
                                    opts: {
                                        iframe: {
                                            preload: !1
                                        }
                                    }
                                })) : "#" === l.charAt(0) && (s = "inline")),
                        s ? u.type = s : i.trigger("objectNeedsType", u),
                        u.contentType || (u.contentType = n.inArray(u.type, ["html", "inline", "ajax"]) > -1 ? "html" : u.type),
                        u.index = i.group.length,
                        "auto" == u.opts.smallBtn && (u.opts.smallBtn = n.inArray(u.type, ["html", "inline", "ajax"]) > -1),
                        "auto" === u.opts.toolbar && (u.opts.toolbar = !u.opts.smallBtn),
                        u.$thumb = u.opts.$thumb || null,
                        u.opts.$trigger && u.index === i.opts.index && (u.$thumb = u.opts.$trigger.find("img:first"),
                            u.$thumb.length && (u.opts.$orig = u.opts.$trigger)),
                        u.$thumb && u.$thumb.length || !u.opts.$orig || (u.$thumb = u.opts.$orig.find("img:first")),
                        u.$thumb && !u.$thumb.length && (u.$thumb = null),
                        u.thumb = u.opts.thumb || (u.$thumb ? u.$thumb[0].src : null),
                        "function" === n.type(u.opts.caption) && (u.opts.caption = u.opts.caption.apply(t, [i, u])),
                        "function" === n.type(i.opts.caption) && (u.opts.caption = i.opts.caption.apply(t, [i, u])),
                        u.opts.caption instanceof n || (u.opts.caption = u.opts.caption === o ? "" : u.opts.caption + ""),
                        "ajax" === u.type && (c = l.split(/\s+/, 2),
                            c.length > 1 && (u.src = c.shift(),
                                u.opts.filter = c.shift())),
                        u.opts.modal && (u.opts = n.extend(!0, u.opts, {
                            trapFocus: !0,
                            infobar: 0,
                            toolbar: 0,
                            smallBtn: 0,
                            keyboard: 0,
                            slideShow: 0,
                            fullScreen: 0,
                            thumbs: 0,
                            touch: 0,
                            clickContent: !1,
                            clickSlide: !1,
                            clickOutside: !1,
                            dblclickContent: !1,
                            dblclickSlide: !1,
                            dblclickOutside: !1
                        })),
                        i.group.push(u)
                }),
                    Object.keys(i.slides).length && (i.updateControls(),
                        (t = i.Thumbs) && t.isActive && (t.create(),
                            t.focus()))
            },
            addEvents: function () {
                var t = this;
                t.removeEvents(),
                    t.$refs.container.on("click.fb-close", "[data-fancybox-close]", function (e) {
                        e.stopPropagation(),
                            e.preventDefault(),
                            t.close(e)
                    }).on("touchstart.fb-prev click.fb-prev", "[data-fancybox-prev]", function (e) {
                        e.stopPropagation(),
                            e.preventDefault(),
                            t.previous()
                    }).on("touchstart.fb-next click.fb-next", "[data-fancybox-next]", function (e) {
                        e.stopPropagation(),
                            e.preventDefault(),
                            t.next()
                    }).on("click.fb", "[data-fancybox-zoom]", function (e) {
                        t[t.isScaledDown() ? "scaleToActual" : "scaleToFit"]()
                    }),
                    s.on("orientationchange.fb resize.fb", function (e) {
                        e && e.originalEvent && "resize" === e.originalEvent.type ? (t.requestId && d(t.requestId),
                            t.requestId = u(function () {
                                t.update(e)
                            })) : (t.current && "iframe" === t.current.type && t.$refs.stage.hide(),
                                setTimeout(function () {
                                    t.$refs.stage.show(),
                                        t.update(e)
                                }, n.fancybox.isMobile ? 600 : 250))
                    }),
                    a.on("keydown.fb", function (e) {
                        var o = n.fancybox ? n.fancybox.getInstance() : null
                            , i = o.current
                            , r = e.keyCode || e.which;
                        if (9 == r)
                            return void (i.opts.trapFocus && t.focus(e));
                        if (!(!i.opts.keyboard || e.ctrlKey || e.altKey || e.shiftKey || n(e.target).is("input,textarea,video,audio")))
                            return 8 === r || 27 === r ? (e.preventDefault(),
                                void t.close(e)) : 37 === r || 38 === r ? (e.preventDefault(),
                                    void t.previous()) : 39 === r || 40 === r ? (e.preventDefault(),
                                        void t.next()) : void t.trigger("afterKeydown", e, r)
                    }),
                    t.group[t.currIndex].opts.idleTime && (t.idleSecondsCounter = 0,
                        a.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle", function (e) {
                            t.idleSecondsCounter = 0,
                                t.isIdle && t.showControls(),
                                t.isIdle = !1
                        }),
                        t.idleInterval = e.setInterval(function () {
                            ++t.idleSecondsCounter >= t.group[t.currIndex].opts.idleTime && !t.isDragging && (t.isIdle = !0,
                                t.idleSecondsCounter = 0,
                                t.hideControls())
                        }, 1e3))
            },
            removeEvents: function () {
                var t = this;
                s.off("orientationchange.fb resize.fb"),
                    a.off("keydown.fb .fb-idle"),
                    this.$refs.container.off(".fb-close .fb-prev .fb-next"),
                    t.idleInterval && (e.clearInterval(t.idleInterval),
                        t.idleInterval = null)
            },
            previous: function (e) {
                return this.jumpTo(this.currPos - 1, e)
            },
            next: function (e) {
                return this.jumpTo(this.currPos + 1, e)
            },
            jumpTo: function (e, t) {
                var i, r, s, a, l, c, u, d, p, h = this, g = h.group.length;
                if (!(h.isDragging || h.isClosing || h.isAnimating && h.firstRun)) {
                    if (e = parseInt(e, 10),
                        !(s = h.current ? h.current.opts.loop : h.opts.loop) && (e < 0 || e >= g))
                        return !1;
                    if (i = h.firstRun = !Object.keys(h.slides).length,
                        l = h.current,
                        h.prevIndex = h.currIndex,
                        h.prevPos = h.currPos,
                        a = h.createSlide(e),
                        g > 1 && ((s || a.index < g - 1) && h.createSlide(e + 1),
                            (s || a.index > 0) && h.createSlide(e - 1)),
                        h.current = a,
                        h.currIndex = a.index,
                        h.currPos = a.pos,
                        h.trigger("beforeShow", i),
                        h.updateControls(),
                        a.forcedDuration = o,
                        n.isNumeric(t) ? a.forcedDuration = t : t = a.opts[i ? "animationDuration" : "transitionDuration"],
                        t = parseInt(t, 10),
                        r = h.isMoved(a),
                        a.$slide.addClass("fancybox-slide--current"),
                        i)
                        return a.opts.animationEffect && t && h.$refs.container.css("transition-duration", t + "ms"),
                            h.$refs.container.addClass("fancybox-is-open").trigger("focus"),
                            h.loadSlide(a),
                            void h.preload("image");
                    c = n.fancybox.getTranslate(l.$slide),
                        u = n.fancybox.getTranslate(h.$refs.stage),
                        n.each(h.slides, function (e, t) {
                            n.fancybox.stop(t.$slide, !0)
                        }),
                        l.pos !== a.pos && (l.isComplete = !1),
                        l.$slide.removeClass("fancybox-slide--complete fancybox-slide--current"),
                        r ? (p = c.left - (l.pos * c.width + l.pos * l.opts.gutter),
                            n.each(h.slides, function (e, o) {
                                o.$slide.removeClass("fancybox-animated").removeClass(function (e, t) {
                                    return (t.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ")
                                });
                                var i = o.pos * c.width + o.pos * o.opts.gutter;
                                n.fancybox.setTranslate(o.$slide, {
                                    top: 0,
                                    left: i - u.left + p
                                }),
                                    o.pos !== a.pos && o.$slide.addClass("fancybox-slide--" + (o.pos > a.pos ? "next" : "previous")),
                                    f(o.$slide),
                                    n.fancybox.animate(o.$slide, {
                                        top: 0,
                                        left: (o.pos - a.pos) * c.width + (o.pos - a.pos) * o.opts.gutter
                                    }, t, function () {
                                        o.$slide.css({
                                            transform: "",
                                            opacity: ""
                                        }).removeClass("fancybox-slide--next fancybox-slide--previous"),
                                            o.pos === h.currPos && h.complete()
                                    })
                            })) : t && a.opts.transitionEffect && (d = "fancybox-animated fancybox-fx-" + a.opts.transitionEffect,
                                l.$slide.addClass("fancybox-slide--" + (l.pos > a.pos ? "next" : "previous")),
                                n.fancybox.animate(l.$slide, d, t, function () {
                                    l.$slide.removeClass(d).removeClass("fancybox-slide--next fancybox-slide--previous")
                                }, !1)),
                        a.isLoaded ? h.revealContent(a) : h.loadSlide(a),
                        h.preload("image")
                }
            },
            createSlide: function (e) {
                var t, o, i = this;
                return o = e % i.group.length,
                    o = o < 0 ? i.group.length + o : o,
                    !i.slides[e] && i.group[o] && (t = n('<div class="fancybox-slide"></div>').appendTo(i.$refs.stage),
                        i.slides[e] = n.extend(!0, {}, i.group[o], {
                            pos: e,
                            $slide: t,
                            isLoaded: !1
                        }),
                        i.updateSlide(i.slides[e])),
                    i.slides[e]
            },
            scaleToActual: function (e, t, i) {
                var r, s, a, l, c, u = this, d = u.current, p = d.$content, f = n.fancybox.getTranslate(d.$slide).width, h = n.fancybox.getTranslate(d.$slide).height, g = d.width, v = d.height;
                u.isAnimating || u.isMoved() || !p || "image" != d.type || !d.isLoaded || d.hasError || (u.isAnimating = !0,
                    n.fancybox.stop(p),
                    e = e === o ? .5 * f : e,
                    t = t === o ? .5 * h : t,
                    r = n.fancybox.getTranslate(p),
                    r.top -= n.fancybox.getTranslate(d.$slide).top,
                    r.left -= n.fancybox.getTranslate(d.$slide).left,
                    l = g / r.width,
                    c = v / r.height,
                    s = .5 * f - .5 * g,
                    a = .5 * h - .5 * v,
                    g > f && (s = r.left * l - (e * l - e),
                        s > 0 && (s = 0),
                        s < f - g && (s = f - g)),
                    v > h && (a = r.top * c - (t * c - t),
                        a > 0 && (a = 0),
                        a < h - v && (a = h - v)),
                    u.updateCursor(g, v),
                    n.fancybox.animate(p, {
                        top: a,
                        left: s,
                        scaleX: l,
                        scaleY: c
                    }, i || 366, function () {
                        u.isAnimating = !1
                    }),
                    u.SlideShow && u.SlideShow.isActive && u.SlideShow.stop())
            },
            scaleToFit: function (e) {
                var t, o = this, i = o.current, r = i.$content;
                o.isAnimating || o.isMoved() || !r || "image" != i.type || !i.isLoaded || i.hasError || (o.isAnimating = !0,
                    n.fancybox.stop(r),
                    t = o.getFitPos(i),
                    o.updateCursor(t.width, t.height),
                    n.fancybox.animate(r, {
                        top: t.top,
                        left: t.left,
                        scaleX: t.width / r.width(),
                        scaleY: t.height / r.height()
                    }, e || 366, function () {
                        o.isAnimating = !1
                    }))
            },
            getFitPos: function (e) {
                var t, o, i, r, s = this, a = e.$content, l = e.$slide, c = e.width || e.opts.width, u = e.height || e.opts.height, d = {};
                return !!(e.isLoaded && a && a.length) && (t = n.fancybox.getTranslate(s.$refs.stage).width,
                    o = n.fancybox.getTranslate(s.$refs.stage).height,
                    t -= parseFloat(l.css("paddingLeft")) + parseFloat(l.css("paddingRight")) + parseFloat(a.css("marginLeft")) + parseFloat(a.css("marginRight")),
                    o -= parseFloat(l.css("paddingTop")) + parseFloat(l.css("paddingBottom")) + parseFloat(a.css("marginTop")) + parseFloat(a.css("marginBottom")),
                    c && u || (c = t,
                        u = o),
                    i = Math.min(1, t / c, o / u),
                    c *= i,
                    u *= i,
                    c > t - .5 && (c = t),
                    u > o - .5 && (u = o),
                    "image" === e.type ? (d.top = Math.floor(.5 * (o - u)) + parseFloat(l.css("paddingTop")),
                        d.left = Math.floor(.5 * (t - c)) + parseFloat(l.css("paddingLeft"))) : "video" === e.contentType && (r = e.opts.width && e.opts.height ? c / u : e.opts.ratio || 16 / 9,
                            u > c / r ? u = c / r : c > u * r && (c = u * r)),
                    d.width = c,
                    d.height = u,
                    d)
            },
            update: function (e) {
                var t = this;
                n.each(t.slides, function (n, o) {
                    t.updateSlide(o, e)
                })
            },
            updateSlide: function (e, t) {
                var o = this
                    , i = e && e.$content
                    , r = e.width || e.opts.width
                    , s = e.height || e.opts.height
                    , a = e.$slide;
                o.adjustCaption(e),
                    i && (r || s || "video" === e.contentType) && !e.hasError && (n.fancybox.stop(i),
                        n.fancybox.setTranslate(i, o.getFitPos(e)),
                        e.pos === o.currPos && (o.isAnimating = !1,
                            o.updateCursor())),
                    o.adjustLayout(e),
                    a.length && (a.trigger("refresh"),
                        e.pos === o.currPos && o.$refs.toolbar.add(o.$refs.navigation.find(".fancybox-button--arrow_right")).toggleClass("compensate-for-scrollbar", a.get(0).scrollHeight > a.get(0).clientHeight)),
                    o.trigger("onUpdate", e, t)
            },
            centerSlide: function (e) {
                var t = this
                    , i = t.current
                    , r = i.$slide;
                !t.isClosing && i && (r.siblings().css({
                    transform: "",
                    opacity: ""
                }),
                    r.parent().children().removeClass("fancybox-slide--previous fancybox-slide--next"),
                    n.fancybox.animate(r, {
                        top: 0,
                        left: 0,
                        opacity: 1
                    }, e === o ? 0 : e, function () {
                        r.css({
                            transform: "",
                            opacity: ""
                        }),
                            i.isComplete || t.complete()
                    }, !1))
            },
            isMoved: function (e) {
                var t, o, i = e || this.current;
                return !!i && (o = n.fancybox.getTranslate(this.$refs.stage),
                    t = n.fancybox.getTranslate(i.$slide),
                    !i.$slide.hasClass("fancybox-animated") && (Math.abs(t.top - o.top) > .5 || Math.abs(t.left - o.left) > .5))
            },
            updateCursor: function (e, t) {
                var o, i, r = this, s = r.current, a = r.$refs.container;
                s && !r.isClosing && r.Guestures && (a.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-zoomOut fancybox-can-swipe fancybox-can-pan"),
                    o = r.canPan(e, t),
                    i = !!o || r.isZoomable(),
                    a.toggleClass("fancybox-is-zoomable", i),
                    n("[data-fancybox-zoom]").prop("disabled", !i),
                    o ? a.addClass("fancybox-can-pan") : i && ("zoom" === s.opts.clickContent || n.isFunction(s.opts.clickContent) && "zoom" == s.opts.clickContent(s)) ? a.addClass("fancybox-can-zoomIn") : s.opts.touch && (s.opts.touch.vertical || r.group.length > 1) && "video" !== s.contentType && a.addClass("fancybox-can-swipe"))
            },
            isZoomable: function () {
                var e, t = this, n = t.current;
                if (n && !t.isClosing && "image" === n.type && !n.hasError) {
                    if (!n.isLoaded)
                        return !0;
                    if ((e = t.getFitPos(n)) && (n.width > e.width || n.height > e.height))
                        return !0
                }
                return !1
            },
            isScaledDown: function (e, t) {
                var i = this
                    , r = !1
                    , s = i.current
                    , a = s.$content;
                return e !== o && t !== o ? r = e < s.width && t < s.height : a && (r = n.fancybox.getTranslate(a),
                    r = r.width < s.width && r.height < s.height),
                    r
            },
            canPan: function (e, t) {
                var i = this
                    , r = i.current
                    , s = null
                    , a = !1;
                return "image" === r.type && (r.isComplete || e && t) && !r.hasError && (a = i.getFitPos(r),
                    e !== o && t !== o ? s = {
                        width: e,
                        height: t
                    } : r.isComplete && (s = n.fancybox.getTranslate(r.$content)),
                    s && a && (a = Math.abs(s.width - a.width) > 1.5 || Math.abs(s.height - a.height) > 1.5)),
                    a
            },
            loadSlide: function (e) {
                var t, o, i, r = this;
                if (!e.isLoading && !e.isLoaded) {
                    if (e.isLoading = !0,
                        !1 === r.trigger("beforeLoad", e))
                        return e.isLoading = !1,
                            !1;
                    switch (t = e.type,
                    o = e.$slide,
                    o.off("refresh").trigger("onReset").addClass(e.opts.slideClass),
                    t) {
                        case "image":
                            r.setImage(e);
                            break;
                        case "iframe":
                            r.setIframe(e);
                            break;
                        case "html":
                            r.setContent(e, e.src || e.content);
                            break;
                        case "video":
                            r.setContent(e, e.opts.video.tpl.replace(/\{\{src\}\}/gi, e.src).replace("{{format}}", e.opts.videoFormat || e.opts.video.format || "").replace("{{poster}}", e.thumb || ""));
                            break;
                        case "inline":
                            n(e.src).length ? r.setContent(e, n(e.src)) : r.setError(e);
                            break;
                        case "ajax":
                            r.showLoading(e),
                                i = n.ajax(n.extend({}, e.opts.ajax.settings, {
                                    url: e.src,
                                    success: function (t, n) {
                                        "success" === n && r.setContent(e, t)
                                    },
                                    error: function (t, n) {
                                        t && "abort" !== n && r.setError(e)
                                    }
                                })),
                                o.one("onReset", function () {
                                    i.abort()
                                });
                            break;
                        default:
                            r.setError(e)
                    }
                    return !0
                }
            },
            setImage: function (e) {
                var o, i = this;
                setTimeout(function () {
                    var t = e.$image;
                    i.isClosing || !e.isLoading || t && t.length && t[0].complete || e.hasError || i.showLoading(e)
                }, 50),
                    i.checkSrcset(e),
                    e.$content = n('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(e.$slide.addClass("fancybox-slide--image")),
                    !1 !== e.opts.preload && e.opts.width && e.opts.height && e.thumb && (e.width = e.opts.width,
                        e.height = e.opts.height,
                        o = t.createElement("img"),
                        o.onerror = function () {
                            n(this).remove(),
                                e.$ghost = null
                        }
                        ,
                        o.onload = function () {
                            i.afterLoad(e)
                        }
                        ,
                        e.$ghost = n(o).addClass("fancybox-image").appendTo(e.$content).attr("src", e.thumb)),
                    i.setBigImage(e)
            },
            checkSrcset: function (t) {
                var n, o, i, r, s = t.opts.srcset || t.opts.image.srcset;
                if (s) {
                    i = e.devicePixelRatio || 1,
                        r = e.innerWidth * i,
                        o = s.split(",").map(function (e) {
                            var t = {};
                            return e.trim().split(/\s+/).forEach(function (e, n) {
                                var o = parseInt(e.substring(0, e.length - 1), 10);
                                if (0 === n)
                                    return t.url = e;
                                o && (t.value = o,
                                    t.postfix = e[e.length - 1])
                            }),
                                t
                        }),
                        o.sort(function (e, t) {
                            return e.value - t.value
                        });
                    for (var a = 0; a < o.length; a++) {
                        var l = o[a];
                        if ("w" === l.postfix && l.value >= r || "x" === l.postfix && l.value >= i) {
                            n = l;
                            break
                        }
                    }
                    !n && o.length && (n = o[o.length - 1]),
                        n && (t.src = n.url,
                            t.width && t.height && "w" == n.postfix && (t.height = t.width / t.height * n.value,
                                t.width = n.value),
                            t.opts.srcset = s)
                }
            },
            setBigImage: function (e) {
                var o = this
                    , i = t.createElement("img")
                    , r = n(i);
                e.$image = r.one("error", function () {
                    o.setError(e)
                }).one("load", function () {
                    var t;
                    e.$ghost || (o.resolveImageSlideSize(e, this.naturalWidth, this.naturalHeight),
                        o.afterLoad(e)),
                        o.isClosing || (e.opts.srcset && (t = e.opts.sizes,
                            t && "auto" !== t || (t = (e.width / e.height > 1 && s.width() / s.height() > 1 ? "100" : Math.round(e.width / e.height * 100)) + "vw"),
                            r.attr("sizes", t).attr("srcset", e.opts.srcset)),
                            e.$ghost && setTimeout(function () {
                                e.$ghost && !o.isClosing && e.$ghost.hide()
                            }, Math.min(300, Math.max(1e3, e.height / 1600))),
                            o.hideLoading(e))
                }).addClass("fancybox-image").attr("src", e.src).appendTo(e.$content),
                    (i.complete || "complete" == i.readyState) && r.naturalWidth && r.naturalHeight ? r.trigger("load") : i.error && r.trigger("error")
            },
            resolveImageSlideSize: function (e, t, n) {
                var o = parseInt(e.opts.width, 10)
                    , i = parseInt(e.opts.height, 10);
                e.width = t,
                    e.height = n,
                    o > 0 && (e.width = o,
                        e.height = Math.floor(o * n / t)),
                    i > 0 && (e.width = Math.floor(i * t / n),
                        e.height = i)
            },
            setIframe: function (e) {
                var t, i = this, r = e.opts.iframe, s = e.$slide;
                e.$content = n('<div class="fancybox-content' + (r.preload ? " fancybox-is-hidden" : "") + '"></div>').css(r.css).appendTo(s),
                    s.addClass("fancybox-slide--" + e.contentType),
                    e.$iframe = t = n(r.tpl.replace(/\{rnd\}/g, (new Date).getTime())).attr(r.attr).appendTo(e.$content),
                    r.preload ? (i.showLoading(e),
                        t.on("load.fb error.fb", function (t) {
                            this.isReady = 1,
                                e.$slide.trigger("refresh"),
                                i.afterLoad(e)
                        }),
                        s.on("refresh.fb", function () {
                            var n, i, a = e.$content, l = r.css.width, c = r.css.height;
                            if (1 === t[0].isReady) {
                                try {
                                    n = t.contents(),
                                        i = n.find("body")
                                } catch (e) { }
                                i && i.length && i.children().length && (s.css("overflow", "visible"),
                                    a.css({
                                        width: "100%",
                                        "max-width": "100%",
                                        height: "9999px"
                                    }),
                                    l === o && (l = Math.ceil(Math.max(i[0].clientWidth, i.outerWidth(!0)))),
                                    a.css("width", l || "").css("max-width", ""),
                                    c === o && (c = Math.ceil(Math.max(i[0].clientHeight, i.outerHeight(!0)))),
                                    a.css("height", c || ""),
                                    s.css("overflow", "auto")),
                                    a.removeClass("fancybox-is-hidden")
                            }
                        })) : i.afterLoad(e),
                    t.attr("src", e.src),
                    s.one("onReset", function () {
                        try {
                            n(this).find("iframe").hide().unbind().attr("src", "//about:blank")
                        } catch (e) { }
                        n(this).off("refresh.fb").empty(),
                            e.isLoaded = !1,
                            e.isRevealed = !1
                    })
            },
            setContent: function (e, t) {
                var o = this;
                o.isClosing || (o.hideLoading(e),
                    e.$content && n.fancybox.stop(e.$content),
                    e.$slide.empty(),
                    c(t) && t.parent().length ? ((t.hasClass("fancybox-content") || t.parent().hasClass("fancybox-content")) && t.parents(".fancybox-slide").trigger("onReset"),
                        e.$placeholder = n("<div>").hide().insertAfter(t),
                        t.css("display", "inline-block")) : e.hasError || ("string" === n.type(t) && (t = n("<div>").append(n.trim(t)).contents()),
                            e.opts.filter && (t = n("<div>").html(t).find(e.opts.filter))),
                    e.$slide.one("onReset", function () {
                        n(this).find("video,audio").trigger("pause"),
                            e.$placeholder && (e.$placeholder.after(t.removeClass("fancybox-content").hide()).remove(),
                                e.$placeholder = null),
                            e.$smallBtn && (e.$smallBtn.remove(),
                                e.$smallBtn = null),
                            e.hasError || (n(this).empty(),
                                e.isLoaded = !1,
                                e.isRevealed = !1)
                    }),
                    n(t).appendTo(e.$slide),
                    n(t).is("video,audio") && (n(t).addClass("fancybox-video"),
                        n(t).wrap("<div></div>"),
                        e.contentType = "video",
                        e.opts.width = e.opts.width || n(t).attr("width"),
                        e.opts.height = e.opts.height || n(t).attr("height")),
                    e.$content = e.$slide.children().filter("div,form,main,video,audio,article,.fancybox-content").first(),
                    e.$content.siblings().hide(),
                    e.$content.length || (e.$content = e.$slide.wrapInner("<div></div>").children().first()),
                    e.$content.addClass("fancybox-content"),
                    e.$slide.addClass("fancybox-slide--" + e.contentType),
                    o.afterLoad(e))
            },
            setError: function (e) {
                e.hasError = !0,
                    e.$slide.trigger("onReset").removeClass("fancybox-slide--" + e.contentType).addClass("fancybox-slide--error"),
                    e.contentType = "html",
                    this.setContent(e, this.translate(e, e.opts.errorTpl)),
                    e.pos === this.currPos && (this.isAnimating = !1)
            },
            showLoading: function (e) {
                var t = this;
                (e = e || t.current) && !e.$spinner && (e.$spinner = n(t.translate(t, t.opts.spinnerTpl)).appendTo(e.$slide).hide().fadeIn("fast"))
            },
            hideLoading: function (e) {
                var t = this;
                (e = e || t.current) && e.$spinner && (e.$spinner.stop().remove(),
                    delete e.$spinner)
            },
            afterLoad: function (e) {
                var t = this;
                t.isClosing || (e.isLoading = !1,
                    e.isLoaded = !0,
                    t.trigger("afterLoad", e),
                    t.hideLoading(e),
                    !e.opts.smallBtn || e.$smallBtn && e.$smallBtn.length || (e.$smallBtn = n(t.translate(e, e.opts.btnTpl.smallBtn)).appendTo(e.$content)),
                    e.opts.protect && e.$content && !e.hasError && (e.$content.on("contextmenu.fb", function (e) {
                        return 2 == e.button && e.preventDefault(),
                            !0
                    }),
                        "image" === e.type && n('<div class="fancybox-spaceball"></div>').appendTo(e.$content)),
                    t.adjustCaption(e),
                    t.adjustLayout(e),
                    e.pos === t.currPos && t.updateCursor(),
                    t.revealContent(e))
            },
            adjustCaption: function (e) {
                var t, n = this, o = e || n.current, i = o.opts.caption, r = o.opts.preventCaptionOverlap, s = n.$refs.caption, a = !1;
                s.toggleClass("fancybox-caption--separate", r),
                    r && i && i.length && (o.pos !== n.currPos ? (t = s.clone().appendTo(s.parent()),
                        t.children().eq(0).empty().html(i),
                        a = t.outerHeight(!0),
                        t.empty().remove()) : n.$caption && (a = n.$caption.outerHeight(!0)),
                        o.$slide.css("padding-bottom", a || ""))
            },
            adjustLayout: function (e) {
                var t, n, o, i, r = this, s = e || r.current;
                s.isLoaded && !0 !== s.opts.disableLayoutFix && (s.$content.css("margin-bottom", ""),
                    s.$content.outerHeight() > s.$slide.height() + .5 && (o = s.$slide[0].style["padding-bottom"],
                        i = s.$slide.css("padding-bottom"),
                        parseFloat(i) > 0 && (t = s.$slide[0].scrollHeight,
                            s.$slide.css("padding-bottom", 0),
                            Math.abs(t - s.$slide[0].scrollHeight) < 1 && (n = i),
                            s.$slide.css("padding-bottom", o))),
                    s.$content.css("margin-bottom", n))
            },
            revealContent: function (e) {
                var t, i, r, s, a = this, l = e.$slide, c = !1, u = !1, d = a.isMoved(e), p = e.isRevealed;
                return e.isRevealed = !0,
                    t = e.opts[a.firstRun ? "animationEffect" : "transitionEffect"],
                    r = e.opts[a.firstRun ? "animationDuration" : "transitionDuration"],
                    r = parseInt(e.forcedDuration === o ? r : e.forcedDuration, 10),
                    !d && e.pos === a.currPos && r || (t = !1),
                    "zoom" === t && (e.pos === a.currPos && r && "image" === e.type && !e.hasError && (u = a.getThumbPos(e)) ? c = a.getFitPos(e) : t = "fade"),
                    "zoom" === t ? (a.isAnimating = !0,
                        c.scaleX = c.width / u.width,
                        c.scaleY = c.height / u.height,
                        s = e.opts.zoomOpacity,
                        "auto" == s && (s = Math.abs(e.width / e.height - u.width / u.height) > .1),
                        s && (u.opacity = .1,
                            c.opacity = 1),
                        n.fancybox.setTranslate(e.$content.removeClass("fancybox-is-hidden"), u),
                        f(e.$content),
                        void n.fancybox.animate(e.$content, c, r, function () {
                            a.isAnimating = !1,
                                a.complete()
                        })) : (a.updateSlide(e),
                            t ? (n.fancybox.stop(l),
                                i = "fancybox-slide--" + (e.pos >= a.prevPos ? "next" : "previous") + " fancybox-animated fancybox-fx-" + t,
                                l.addClass(i).removeClass("fancybox-slide--current"),
                                e.$content.removeClass("fancybox-is-hidden"),
                                f(l),
                                "image" !== e.type && e.$content.hide().show(0),
                                void n.fancybox.animate(l, "fancybox-slide--current", r, function () {
                                    l.removeClass(i).css({
                                        transform: "",
                                        opacity: ""
                                    }),
                                        e.pos === a.currPos && a.complete()
                                }, !0)) : (e.$content.removeClass("fancybox-is-hidden"),
                                    p || !d || "image" !== e.type || e.hasError || e.$content.hide().fadeIn("fast"),
                                    void (e.pos === a.currPos && a.complete())))
            },
            getThumbPos: function (e) {
                var t, o, i, r, s, a = !1, l = e.$thumb;
                return !(!l || !g(l[0])) && (t = n.fancybox.getTranslate(l),
                    o = parseFloat(l.css("border-top-width") || 0),
                    i = parseFloat(l.css("border-right-width") || 0),
                    r = parseFloat(l.css("border-bottom-width") || 0),
                    s = parseFloat(l.css("border-left-width") || 0),
                    a = {
                        top: t.top + o,
                        left: t.left + s,
                        width: t.width - i - s,
                        height: t.height - o - r,
                        scaleX: 1,
                        scaleY: 1
                    },
                    t.width > 0 && t.height > 0 && a)
            },
            complete: function () {
                var e, t = this, o = t.current, i = {};
                !t.isMoved() && o.isLoaded && (o.isComplete || (o.isComplete = !0,
                    o.$slide.siblings().trigger("onReset"),
                    t.preload("inline"),
                    f(o.$slide),
                    o.$slide.addClass("fancybox-slide--complete"),
                    n.each(t.slides, function (e, o) {
                        o.pos >= t.currPos - 1 && o.pos <= t.currPos + 1 ? i[o.pos] = o : o && (n.fancybox.stop(o.$slide),
                            o.$slide.off().remove())
                    }),
                    t.slides = i),
                    t.isAnimating = !1,
                    t.updateCursor(),
                    t.trigger("afterShow"),
                    o.opts.video.autoStart && o.$slide.find("video,audio").filter(":visible:first").trigger("play").one("ended", function () {
                        this.webkitExitFullscreen && this.webkitExitFullscreen(),
                            t.next()
                    }),
                    o.opts.autoFocus && "html" === o.contentType && (e = o.$content.find("input[autofocus]:enabled:visible:first"),
                        e.length ? e.trigger("focus") : t.focus(null, !0)),
                    o.$slide.scrollTop(0).scrollLeft(0))
            },
            preload: function (e) {
                var t, n, o = this;
                o.group.length < 2 || (n = o.slides[o.currPos + 1],
                    t = o.slides[o.currPos - 1],
                    t && t.type === e && o.loadSlide(t),
                    n && n.type === e && o.loadSlide(n))
            },
            focus: function (e, o) {
                var i, r, s = this, a = ["a[href]", "area[href]", 'input:not([disabled]):not([type="hidden"]):not([aria-hidden])', "select:not([disabled]):not([aria-hidden])", "textarea:not([disabled]):not([aria-hidden])", "button:not([disabled]):not([aria-hidden])", "iframe", "object", "embed", "video", "audio", "[contenteditable]", '[tabindex]:not([tabindex^="-"])'].join(",");
                s.isClosing || (i = !e && s.current && s.current.isComplete ? s.current.$slide.find("*:visible" + (o ? ":not(.fancybox-close-small)" : "")) : s.$refs.container.find("*:visible"),
                    i = i.filter(a).filter(function () {
                        return "hidden" !== n(this).css("visibility") && !n(this).hasClass("disabled")
                    }),
                    i.length ? (r = i.index(t.activeElement),
                        e && e.shiftKey ? (r < 0 || 0 == r) && (e.preventDefault(),
                            i.eq(i.length - 1).trigger("focus")) : (r < 0 || r == i.length - 1) && (e && e.preventDefault(),
                                i.eq(0).trigger("focus"))) : s.$refs.container.trigger("focus"))
            },
            activate: function () {
                var e = this;
                n(".fancybox-container").each(function () {
                    var t = n(this).data("FancyBox");
                    t && t.id !== e.id && !t.isClosing && (t.trigger("onDeactivate"),
                        t.removeEvents(),
                        t.isVisible = !1)
                }),
                    e.isVisible = !0,
                    (e.current || e.isIdle) && (e.update(),
                        e.updateControls()),
                    e.trigger("onActivate"),
                    e.addEvents()
            },
            close: function (e, t) {
                var o, i, r, s, a, l, c, d = this, p = d.current, h = function () {
                    d.cleanUp(e)
                };
                return !d.isClosing && (d.isClosing = !0,
                    !1 === d.trigger("beforeClose", e) ? (d.isClosing = !1,
                        u(function () {
                            d.update()
                        }),
                        !1) : (d.removeEvents(),
                            r = p.$content,
                            o = p.opts.animationEffect,
                            i = n.isNumeric(t) ? t : o ? p.opts.animationDuration : 0,
                            p.$slide.removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"),
                            !0 !== e ? n.fancybox.stop(p.$slide) : o = !1,
                            p.$slide.siblings().trigger("onReset").remove(),
                            i && d.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing").css("transition-duration", i + "ms"),
                            d.hideLoading(p),
                            d.hideControls(!0),
                            d.updateCursor(),
                            "zoom" !== o || r && i && "image" === p.type && !d.isMoved() && !p.hasError && (c = d.getThumbPos(p)) || (o = "fade"),
                            "zoom" === o ? (n.fancybox.stop(r),
                                s = n.fancybox.getTranslate(r),
                                l = {
                                    top: s.top,
                                    left: s.left,
                                    scaleX: s.width / c.width,
                                    scaleY: s.height / c.height,
                                    width: c.width,
                                    height: c.height
                                },
                                a = p.opts.zoomOpacity,
                                "auto" == a && (a = Math.abs(p.width / p.height - c.width / c.height) > .1),
                                a && (c.opacity = 0),
                                n.fancybox.setTranslate(r, l),
                                f(r),
                                n.fancybox.animate(r, c, i, h),
                                !0) : (o && i ? n.fancybox.animate(p.$slide.addClass("fancybox-slide--previous").removeClass("fancybox-slide--current"), "fancybox-animated fancybox-fx-" + o, i, h) : !0 === e ? setTimeout(h, i) : h(),
                                    !0)))
            },
            cleanUp: function (t) {
                var o, i, r, s = this, a = s.current.opts.$orig;
                s.current.$slide.trigger("onReset"),
                    s.$refs.container.empty().remove(),
                    s.trigger("afterClose", t),
                    s.current.opts.backFocus && (a && a.length && a.is(":visible") || (a = s.$trigger),
                        a && a.length && (i = e.scrollX,
                            r = e.scrollY,
                            a.trigger("focus"),
                            n("html, body").scrollTop(r).scrollLeft(i))),
                    s.current = null,
                    o = n.fancybox.getInstance(),
                    o ? o.activate() : (n("body").removeClass("fancybox-active compensate-for-scrollbar"),
                        n("#fancybox-style-noscroll").remove())
            },
            trigger: function (e, t) {
                var o, i = Array.prototype.slice.call(arguments, 1), r = this, s = t && t.opts ? t : r.current;
                if (s ? i.unshift(s) : s = r,
                    i.unshift(r),
                    n.isFunction(s.opts[e]) && (o = s.opts[e].apply(s, i)),
                    !1 === o)
                    return o;
                "afterClose" !== e && r.$refs ? r.$refs.container.trigger(e + ".fb", i) : a.trigger(e + ".fb", i)
            },
            updateControls: function () {
                var e = this
                    , o = e.current
                    , i = o.index
                    , r = e.$refs.container
                    , s = e.$refs.caption
                    , a = o.opts.caption;
                o.$slide.trigger("refresh"),
                    a && a.length ? (e.$caption = s,
                        s.children().eq(0).html(a)) : e.$caption = null,
                    e.hasHiddenControls || e.isIdle || e.showControls(),
                    r.find("[data-fancybox-count]").html(e.group.length),
                    r.find("[data-fancybox-index]").html(i + 1),
                    r.find("[data-fancybox-prev]").prop("disabled", !o.opts.loop && i <= 0),
                    r.find("[data-fancybox-next]").prop("disabled", !o.opts.loop && i >= e.group.length - 1),
                    "image" === o.type ? r.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href", o.opts.image.src || o.src).show() : o.opts.toolbar && r.find("[data-fancybox-download],[data-fancybox-zoom]").hide(),
                    n(t.activeElement).is(":hidden,[disabled]") && e.$refs.container.trigger("focus")
            },
            hideControls: function (e) {
                var t = this
                    , n = ["infobar", "toolbar", "nav"];
                !e && t.current.opts.preventCaptionOverlap || n.push("caption"),
                    this.$refs.container.removeClass(n.map(function (e) {
                        return "fancybox-show-" + e
                    }).join(" ")),
                    this.hasHiddenControls = !0
            },
            showControls: function () {
                var e = this
                    , t = e.current ? e.current.opts : e.opts
                    , n = e.$refs.container;
                e.hasHiddenControls = !1,
                    e.idleSecondsCounter = 0,
                    n.toggleClass("fancybox-show-toolbar", !(!t.toolbar || !t.buttons)).toggleClass("fancybox-show-infobar", !!(t.infobar && e.group.length > 1)).toggleClass("fancybox-show-caption", !!e.$caption).toggleClass("fancybox-show-nav", !!(t.arrows && e.group.length > 1)).toggleClass("fancybox-is-modal", !!t.modal)
            },
            toggleControls: function () {
                this.hasHiddenControls ? this.showControls() : this.hideControls()
            }
        }),
            n.fancybox = {
                version: "3.5.6",
                defaults: r,
                getInstance: function (e) {
                    var t = n('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox")
                        , o = Array.prototype.slice.call(arguments, 1);
                    return t instanceof v && ("string" === n.type(e) ? t[e].apply(t, o) : "function" === n.type(e) && e.apply(t, o),
                        t)
                },
                open: function (e, t, n) {
                    return new v(e, t, n)
                },
                close: function (e) {
                    var t = this.getInstance();
                    t && (t.close(),
                        !0 === e && this.close(e))
                },
                destroy: function () {
                    this.close(!0),
                        a.add("body").off("click.fb-start", "**")
                },
                isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
                use3d: function () {
                    var n = t.createElement("div");
                    return e.getComputedStyle && e.getComputedStyle(n) && e.getComputedStyle(n).getPropertyValue("transform") && !(t.documentMode && t.documentMode < 11)
                }(),
                getTranslate: function (e) {
                    var t;
                    return !(!e || !e.length) && (t = e[0].getBoundingClientRect(),
                    {
                        top: t.top || 0,
                        left: t.left || 0,
                        width: t.width,
                        height: t.height,
                        opacity: parseFloat(e.css("opacity"))
                    })
                },
                setTranslate: function (e, t) {
                    var n = ""
                        , i = {};
                    if (e && t)
                        return t.left === o && t.top === o || (n = (t.left === o ? e.position().left : t.left) + "px, " + (t.top === o ? e.position().top : t.top) + "px",
                            n = this.use3d ? "translate3d(" + n + ", 0px)" : "translate(" + n + ")"),
                            t.scaleX !== o && t.scaleY !== o ? n += " scale(" + t.scaleX + ", " + t.scaleY + ")" : t.scaleX !== o && (n += " scaleX(" + t.scaleX + ")"),
                            n.length && (i.transform = n),
                            t.opacity !== o && (i.opacity = t.opacity),
                            t.width !== o && (i.width = t.width),
                            t.height !== o && (i.height = t.height),
                            e.css(i)
                },
                animate: function (e, t, i, r, s) {
                    var a, l = this;
                    n.isFunction(i) && (r = i,
                        i = null),
                        l.stop(e),
                        a = l.getTranslate(e),
                        e.on(p, function (c) {
                            (!c || !c.originalEvent || e.is(c.originalEvent.target) && "z-index" != c.originalEvent.propertyName) && (l.stop(e),
                                n.isNumeric(i) && e.css("transition-duration", ""),
                                n.isPlainObject(t) ? t.scaleX !== o && t.scaleY !== o && l.setTranslate(e, {
                                    top: t.top,
                                    left: t.left,
                                    width: a.width * t.scaleX,
                                    height: a.height * t.scaleY,
                                    scaleX: 1,
                                    scaleY: 1
                                }) : !0 !== s && e.removeClass(t),
                                n.isFunction(r) && r(c))
                        }),
                        n.isNumeric(i) && e.css("transition-duration", i + "ms"),
                        n.isPlainObject(t) ? (t.scaleX !== o && t.scaleY !== o && (delete t.width,
                            delete t.height,
                            e.parent().hasClass("fancybox-slide--image") && e.parent().addClass("fancybox-is-scaling")),
                            n.fancybox.setTranslate(e, t)) : e.addClass(t),
                        e.data("timer", setTimeout(function () {
                            e.trigger(p)
                        }, i + 33))
                },
                stop: function (e, t) {
                    e && e.length && (clearTimeout(e.data("timer")),
                        t && e.trigger(p),
                        e.off(p).css("transition-duration", ""),
                        e.parent().removeClass("fancybox-is-scaling"))
                }
            },
            n.fn.fancybox = function (e) {
                var t;
                return e = e || {},
                    t = e.selector || !1,
                    t ? n("body").off("click.fb-start", t).on("click.fb-start", t, {
                        options: e
                    }, i) : this.off("click.fb-start").on("click.fb-start", {
                        items: this,
                        options: e
                    }, i),
                    this
            }
            ,
            a.on("click.fb-start", "[data-fancybox]", i),
            a.on("click.fb-start", "[data-fancybox-trigger]", function (e) {
                n('[data-fancybox="' + n(this).attr("data-fancybox-trigger") + '"]').eq(n(this).attr("data-fancybox-index") || 0).trigger("click.fb-start", {
                    $trigger: n(this)
                })
            }),
            function () {
                var e = ".fancybox-button"
                    , t = "fancybox-focus"
                    , o = null;
                a.on("mousedown mouseup focus blur", e, function (i) {
                    switch (i.type) {
                        case "mousedown":
                            o = n(this);
                            break;
                        case "mouseup":
                            o = null;
                            break;
                        case "focusin":
                            n(e).removeClass(t),
                                n(this).is(o) || n(this).is("[disabled]") || n(this).addClass(t);
                            break;
                        case "focusout":
                            n(e).removeClass(t)
                    }
                })
            }()
    }
}

fancyBoxTestForm();
