$('.projects__item').each(function () {
    var $this = $(this),
        imgSrc = $this.find('.projects__img img').attr('src'),
        styleInlineBgImg = 'background-image: url(' + imgSrc + ');';
    $this.find('.projects__img').attr('style', styleInlineBgImg);
})

$('.js-sorting-current').on('click', function () {
    $('.js-sorting-items').toggleClass('is-show-sorting-items');
})