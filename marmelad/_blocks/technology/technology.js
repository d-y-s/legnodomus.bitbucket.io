function explicitlyInteraction() {

    $('.js-main-point').on('click', function (event) {
        event.preventDefault();
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);

    });
    
    $('.js-explicitly-list-item').on('click', function () {
        var $this = $(this);
        var getPoint = $(this).closest('.technology__item--level-1').find('.technology__explicitly-point').eq($this.index());

        $this
            .addClass('is-active-explicitly-list-item')
            .siblings()
            .removeClass('is-active-explicitly-list-item');
        
        getPoint
            .addClass('is-active-explicitly-point')
            .siblings()
            .removeClass('is-active-explicitly-point');
    });

    $('.js-explicitly-list-item').on('click', function () {
        var $this = $(this);
        var getPoint = $(this).closest('.technology__item--level-2').find('.technology__explicitly-point').eq($this.index());

        $this
            .addClass('is-active-explicitly-list-item')
            .siblings()
            .removeClass('is-active-explicitly-list-item');

        getPoint
            .addClass('is-active-explicitly-point')
            .siblings()
            .removeClass('is-active-explicitly-point');
    });

    $('.js-explicitly-list-item').on('click', function () {
        var $this = $(this);
        var getPoint = $(this).closest('.technology__item--level-3').find('.technology__explicitly-point').eq($this.index());

        $this
            .addClass('is-active-explicitly-list-item')
            .siblings()
            .removeClass('is-active-explicitly-list-item');

        getPoint
            .addClass('is-active-explicitly-point')
            .siblings()
            .removeClass('is-active-explicitly-point');
    });

    $('.js-explicitly-list-item').on('click', function () {
        var $this = $(this);
        var getPoint = $(this).closest('.technology__item--level-4').find('.technology__explicitly-point').eq($this.index());

        $this
            .addClass('is-active-explicitly-list-item')
            .siblings()
            .removeClass('is-active-explicitly-list-item');

        getPoint
            .addClass('is-active-explicitly-point')
            .siblings()
            .removeClass('is-active-explicitly-point');
    });

    $('.js-explicitly-list-item').on('click', function () {
        var $this = $(this);
        var getPoint = $(this).closest('.technology__item--level-5').find('.technology__explicitly-point').eq($this.index());

        $this
            .addClass('is-active-explicitly-list-item')
            .siblings()
            .removeClass('is-active-explicitly-list-item');

        getPoint
            .addClass('is-active-explicitly-point')
            .siblings()
            .removeClass('is-active-explicitly-point');
    });

    $(".js-explicitly-list-item").click(function () {
        var $this = $(this);
        $('html, body').animate({
            scrollTop: $this.closest('.technology__item').offset().top - 50
        }, 1000);
    });
}

explicitlyInteraction();