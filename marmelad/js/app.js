/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
    opts = Object.assign({
        tag: 'div',
        type: 'icons',
        class: '',
        mode: 'inline',
        url: '',
    }, opts);

    let external = '';
    let typeClass = '';

    if (opts.mode === 'external') {
        external = `${opts.url}/sprite.${opts.type}.svg`;
    }

    if (opts.type !== 'icons') {
        typeClass = ` svg-icon--${opts.type}`;
    }

    opts.class = opts.class ? ` ${opts.class}` : '';

    return `
    <${opts.tag} class="svg-icon svg-icon--${name}${typeClass}${opts.class}" aria-hidden="true" focusable="false">
      <svg class="svg-icon__link">
        <use xlink:href="${external}#${name}"></use>
      </svg>
    </${opts.tag}>
  `;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function () {

    'use strict';

    /**
     * определение существования элемента на странице
     */
    $.exists = (selector) => $(selector).length > 0;

    //=require ../_blocks/**/*.js

    $('input[type="file"], input[type="checkbox"], input[type="radio"], select').not('.form-test-popup input[type="radio"], .form-test-popup input[type="checkbox"]').styler({
        filePlaceholder: 'Выберите файл проекта'
    });

    var slideUp = {
        distance: '150%',
        origin: 'bottom',
        opacity: null
    };

    // ScrollReveal().reveal('.headline', slideUp)

    ScrollReveal().reveal('.js-animate-scroll-reveal', { delay: 500, distance: '50px', origin: 'top', reset: true, });

    $(window).on('load resize', function () {
        if ($(window).width() <= 1024) {
            ScrollReveal().destroy();
        }
    })

});
