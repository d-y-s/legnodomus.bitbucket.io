$('.js-recommended-projects').owlCarousel({
    items: 1,
    nav: true,
    dots: false,
    // loop: true,
    onInitialized: initCounterSlide,
    onTranslate: initCounterSlide,
    onTranslated: initCounterSlide,
    navText: ['<i class="left-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>', '<i class="right-arr"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1.29289" y1="31.2929" x2="17.2929" y2="15.2929" stroke="black" stroke-width="2"/><line y1="-1" x2="22.6274" y2="-1" transform="matrix(0.707107 0.707107 0.707107 -0.707107 2 0)" stroke="black" stroke-width="2"/></svg></i>']
});

function initCounterSlide(event) {
    if (!event.namespace) {
        return;
    }
    var slides = event.relatedTarget;
    $('.js-counter').html(slides.relative(slides.current()) + 1 + '<span>' + '<i>' + '</i>' + slides.items().length + '</i>');
}
