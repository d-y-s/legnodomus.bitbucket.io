$('.js-filter-name').on('click', function () {
    $(this).toggleClass('is-active-filter-name')
    $(this).siblings('.js-check-items').stop().slideToggle(100);
})

$('.js-filter-title').on('click', function () {
    $(this).siblings('.filter__items').stop().slideToggle(100);
})

$(window).on('load resize', function () {
    if ($(window).width() >= 640) {
        $('.filter__items').css('display', 'flex')
    }
})
